package beans;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Orders {
	// List<Orders>, but instructions infer that it means this, right?...
	private ArrayList<Order> orders;
	
	public Orders() {
		
	}

	public ArrayList<Order> getOrders() {
		return this.orders;
	}

	public void setOrders(ArrayList<Order> orders) {
		this.orders = orders;
	}
}
